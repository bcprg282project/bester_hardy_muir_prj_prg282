﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bussiness_Logic;

namespace Bester_C_Hardy_T_Muir_T_PRJ_PRG282_VanWyk_Q
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }
        List<User> UserList = new List<User>();

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            //Sets password textbox that password cannot be seen, making dots instead
            txtPassword.UseSystemPasswordChar = true;
            btnShowPassword.Text = "Show Password";

            User user = new User();
            UserList = user.ReadData();

        }

        private void BtnShowPassword_Click(object sender, EventArgs e)
        {
            //if the button will be clicked, the pasword will be shown, by setting the useSystemPasswordChar to false
            //Also changing the button text accordingly
            //If the button is clicked again the password will be hidden, by setting the useSystemPasswordChar to true
            //And also changing button text accordingly
            if (txtPassword.UseSystemPasswordChar)
            {
                txtPassword.UseSystemPasswordChar = false;
                btnShowPassword.Text = "Hide Password";
            }
            else
            {
                txtPassword.UseSystemPasswordChar = true;
                btnShowPassword.Text = "Show Password";
            }
        }

        //Validates user details aswell as display message on incorrect details
        private void BtnLogin_Click(object sender, EventArgs e)
        {
            string rank = "";
            bool flag1 = false, flag2 = false ;
            foreach (User item in UserList)
            {
                if (txtUsername.Text==item.Username && txtPassword.Text==item.Password)
                {
                    if (item.CurrentRank == "Officer")
                    {
                        flag1 = true;
                        rank = item.CurrentRank;
                        break;
                    }
                    else
                    { 
                        flag2 = true;
                        rank = item.CurrentRank;
                        break;
                    }
                }
            }

            if (flag1)
            {
                frmEditUsers frmEditUsers = new frmEditUsers();
                frmEditUsers.Show();
                
                this.Hide();
            }
            else if (flag2)
            {
                frmCheckMessages frmCheckMessages = new frmCheckMessages(rank);
                frmCheckMessages.Show();
                
                this.Hide();
            }
            else
            {
                MessageBox.Show("Incorrect details", "Login Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPassword.Clear();
                txtUsername.Focus();
                txtUsername.SelectAll();
            }
        }

        //Display messages on mouse hover over components
        private void BtnLogin_MouseHover(object sender, EventArgs e)
        {
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(this.btnLogin, "Login");
        }

        private void BtnShowPassword_MouseHover(object sender, EventArgs e)
        {
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(this.btnShowPassword, "Shows current password enetered");
        }

        private void TxtUsername_MouseHover(object sender, EventArgs e)
        {
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(this.btnShowPassword, "Please enter username here");
        }
        private void TxtPassword_MouseHover(object sender, EventArgs e)
        {
            ToolTip ToolTip1 = new ToolTip();
            ToolTip1.SetToolTip(this.btnShowPassword, "Please enter password here");
        }
    }
}
