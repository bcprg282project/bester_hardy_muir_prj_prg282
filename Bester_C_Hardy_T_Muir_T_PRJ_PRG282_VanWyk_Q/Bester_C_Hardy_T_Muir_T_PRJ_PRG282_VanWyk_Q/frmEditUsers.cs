﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bussiness_Logic;

namespace Bester_C_Hardy_T_Muir_T_PRJ_PRG282_VanWyk_Q
{
    public partial class frmEditUsers : Form
    {
        public frmEditUsers()
        {
            InitializeComponent();
            txtGender.Enabled = false;
            txtAge.Enabled = false;
        }
        //globally instansiate my userlist, aswell as the binding source
        BindingSource bs = new BindingSource();
        List<User> UserList = new List<User>();
        private void FrmEditUsers_Load(object sender, EventArgs e)
        {
            RefreshChanges();
        }

        //This method will re populate my user list aswell as refresh the datagridview's items
        //also will populate comboboxes
        public void RefreshChanges()
        {
            User user = new User();
            UserList = user.ReadData();
            bs.DataSource = UserList;
            dgvDisplay.DataSource = bs;
            this.dgvDisplay.Columns["UserID"].Visible = false;//user may not know the generated id 

            foreach (User item in UserList)
            {
                if (!cbxFilterGender.Items.Contains(item.Gender))
                {
                    cbxFilterGender.Items.Add(item.Gender);
                }

                if (!cbxFilterRanking.Items.Contains(item.CurrentRank))
                {
                    cbxFilterRanking.Items.Add(item.CurrentRank);
                }
            }
            bs.MoveFirst();
        }

        private void CheckForIncomingMessagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Instantiate object and
            //hides the current form and takes user to the new form
            frmCheckMessages frmCheckMessages = new frmCheckMessages();
            this.Hide();
            frmCheckMessages.Show();
        }

        private void InsertUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //set textboxes to variables and user variables to pass through as parameters
            //to the insert data method in user class, which will pass it to the dbaccess class
            //for this database and then finally write to the database
            string fName = txtName.Text;
            string lName = txtSurname.Text;
            string iDNume = txtIdNumber.Text;
            string ranking = txtRanking.Text;
            string userName = txtUsername.Text;
            string password = txtPassword.Text;
            if (fName=="" || lName=="" || iDNume=="" || ranking=="" || userName=="" || password=="")
            {
                MessageBox.Show("Fields can't be empty", "Insert error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (iDNume.Length!=13)
            {
                MessageBox.Show("Id number needs to contain 13 numbers","Insert error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {
                User user = new User();
                user.InsertData(fName, lName, iDNume, ranking, userName, password);
                RefreshChanges();
            }
        }

        private void DeleteUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Method will delete selected user by passing the userID through
            //to the user class corresponding method which will then pass
            //it through to the dbaccess class of current db and will then finally delete from the db
            int userID = ((User)bs.Current).UserID;
            User user = new User();
           
            DialogResult result= MessageBox.Show("Are you sure you want to delete user","Delete Conformation",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
            if (result == DialogResult.Yes) 
            {
                user.DeleteData(userID);
            }
            RefreshChanges();
        }

        private void UpdateUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Method will delete selected user by passing the needed variables as parameters through
            //to the user class corresponding method which will then pass
            //it through to the dbaccess class of current db and will then finally Update to the db
            string fName = txtName.Text;
            string lName = txtSurname.Text;
            string ranking = txtRanking.Text;
            string userName = txtUsername.Text;
            string password = txtPassword.Text;
            int userID = ((User)bs.Current).UserID;

            User user = new User();
            user.UpdateData(userID,fName, lName, ranking, userName, password);
            RefreshChanges();
        }

        private void DgvDisplay_SelectionChanged(object sender, EventArgs e)
        {
            //Passing currently selected item in datagrid view to text boxes for editing purposes
            User currentUser = (User)bs.Current;
            if (currentUser != null)
            {
                txtName.Text = currentUser.FirstName;
                txtSurname.Text = currentUser.LastName;
                txtIdNumber.Text = currentUser.IDNumber;
                txtRanking.Text = currentUser.CurrentRank;
                txtUsername.Text = currentUser.Username;
                txtPassword.Text = currentUser.Password;
                txtAge.Text = currentUser.Age.ToString();
                txtGender.Text = currentUser.Gender;
            }
        }

        //Clears all textboxes so editing can happen better
        private void BtnClear_Click(object sender, EventArgs e)
        {
            txtName.Clear();
            txtSurname.Clear();
            txtIdNumber.Clear();
            txtRanking.Clear();
            txtUsername.Clear();
            txtPassword.Clear();
            txtAge.Clear();
            txtGender.Clear();
        }

        //Move inside of the datagridview
        private void BtnFirst_Click(object sender, EventArgs e){bs.MoveFirst();}
        private void BtnPrevious_Click(object sender, EventArgs e){bs.MovePrevious();}
        private void BtnNext_Click(object sender, EventArgs e){bs.MoveNext();}
        private void BtnLast_Click(object sender, EventArgs e){bs.MoveLast();}

        
        private void BntSearch_Click(object sender, EventArgs e)
        {
            //Search for an specific user by their firstname
            User user = new User();
            List<User> temp = new List<User>();
            temp = user.SearchData(txtSearch.Text.ToUpper());
            bs.DataSource = temp;
        }

        private void BtnExitApplication_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void BtnFilter_Click(object sender, EventArgs e)
        {
            int startAge = 0, endAge = 0;
            string filterGender = "",filterRank = "";
            User user = new User();
            List<User> filterList = new List<User>();
            try
            {
                filterGender = cbxFilterGender.SelectedItem.ToString();
                filterRank = cbxFilterRanking.SelectedItem.ToString();
                startAge = int.Parse(txtFilterAgeStart.Text);
                endAge = int.Parse(txtFilterAgeEnd.Text);

                filterList = user.FilterData(startAge, endAge, filterRank, filterGender);
                bs.DataSource = filterList;
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Please select correct gender and rankking you want to filter by");
            }
            catch (FormatException)
            {
                MessageBox.Show("Please insert valid age range");
            }
        }

        private void BtnResetFilter_Click(object sender, EventArgs e)
        {
            bs.DataSource = UserList;
            txtFilterAgeEnd.Clear();
            txtFilterAgeStart.Clear();
            cbxFilterGender.Text = "";
            cbxFilterRanking.Text = "";
            txtSearch.Clear();
        }

        //Filter out characters to prevent gigo principle
        private void TxtIdNumber_KeyPress(object sender, KeyPressEventArgs e) { e.Handled = !char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar); }
        private void TxtName_KeyPress(object sender, KeyPressEventArgs e) { e.Handled = !char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar); }
        private void TxtSurname_KeyPress(object sender, KeyPressEventArgs e) { e.Handled = !char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar); }
        private void TxtRanking_KeyPress(object sender, KeyPressEventArgs e) { e.Handled = !char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar); }
        private void TxtUsername_KeyPress(object sender, KeyPressEventArgs e) { e.Handled = !char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) && !char.IsDigit(e.KeyChar); }
        private void TxtPassword_KeyPress(object sender, KeyPressEventArgs e) { e.Handled = !char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) && !char.IsDigit(e.KeyChar); }
        private void TxtFilterAgeStart_KeyPress(object sender, KeyPressEventArgs e) { e.Handled = !char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar); }
        private void TxtFilterAgeEnd_KeyPress(object sender, KeyPressEventArgs e) { e.Handled = !char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar); }
        private void TxtSearch_KeyPress(object sender, KeyPressEventArgs e) { e.Handled = !char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar); }

        //Unused events
        private void GroupBox1_Enter(object sender, EventArgs e) { }
        private void CbxFilterGender_SelectionChangeCommitted(object sender, EventArgs e) { }
        private void CbxFilterRanking_SelectionChangeCommitted(object sender, EventArgs e) { }
        private void TxtSearch_TextChanged(object sender, EventArgs e) { }

        
    }
}
