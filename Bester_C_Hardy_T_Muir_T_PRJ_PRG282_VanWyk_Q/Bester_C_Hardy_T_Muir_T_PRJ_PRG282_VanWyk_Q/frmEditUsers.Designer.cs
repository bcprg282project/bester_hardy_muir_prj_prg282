﻿namespace Bester_C_Hardy_T_Muir_T_PRJ_PRG282_VanWyk_Q
{
    partial class frmEditUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msOptions = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForIncomingMessagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvDisplay = new System.Windows.Forms.DataGridView();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblRanking = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.txtIdNumber = new System.Windows.Forms.TextBox();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.gbxMoveBetweenRecords = new System.Windows.Forms.GroupBox();
            this.gbxRecordDetails = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtGender = new System.Windows.Forms.TextBox();
            this.txtRanking = new System.Windows.Forms.TextBox();
            this.gbxFilter = new System.Windows.Forms.GroupBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.txtFilterAgeEnd = new System.Windows.Forms.TextBox();
            this.txtFilterAgeStart = new System.Windows.Forms.TextBox();
            this.lblFilterAgeEnd = new System.Windows.Forms.Label();
            this.lbFilterAgeStart = new System.Windows.Forms.Label();
            this.cbxFilterRanking = new System.Windows.Forms.ComboBox();
            this.cbxFilterGender = new System.Windows.Forms.ComboBox();
            this.lblFilterAge = new System.Windows.Forms.Label();
            this.lblFilterRanking = new System.Windows.Forms.Label();
            this.lblFilterGender = new System.Windows.Forms.Label();
            this.btnExitApplication = new System.Windows.Forms.Button();
            this.gbxSearch = new System.Windows.Forms.GroupBox();
            this.bntSearch = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblSearch = new System.Windows.Forms.Label();
            this.btnResetFilter = new System.Windows.Forms.Button();
            this.msOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDisplay)).BeginInit();
            this.gbxMoveBetweenRecords.SuspendLayout();
            this.gbxRecordDetails.SuspendLayout();
            this.gbxFilter.SuspendLayout();
            this.gbxSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // msOptions
            // 
            this.msOptions.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.msOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.msOptions.Location = new System.Drawing.Point(0, 0);
            this.msOptions.Name = "msOptions";
            this.msOptions.Size = new System.Drawing.Size(1409, 28);
            this.msOptions.TabIndex = 0;
            this.msOptions.Text = "menuStrip1";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editUserToolStripMenuItem,
            this.checkForIncomingMessagesToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(73, 24);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // editUserToolStripMenuItem
            // 
            this.editUserToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insertUserToolStripMenuItem,
            this.deleteUserToolStripMenuItem,
            this.updateUserToolStripMenuItem});
            this.editUserToolStripMenuItem.Name = "editUserToolStripMenuItem";
            this.editUserToolStripMenuItem.Size = new System.Drawing.Size(280, 26);
            this.editUserToolStripMenuItem.Text = "Edit User";
            // 
            // insertUserToolStripMenuItem
            // 
            this.insertUserToolStripMenuItem.Name = "insertUserToolStripMenuItem";
            this.insertUserToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.insertUserToolStripMenuItem.Text = "Insert User";
            this.insertUserToolStripMenuItem.Click += new System.EventHandler(this.InsertUserToolStripMenuItem_Click);
            // 
            // deleteUserToolStripMenuItem
            // 
            this.deleteUserToolStripMenuItem.Name = "deleteUserToolStripMenuItem";
            this.deleteUserToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.deleteUserToolStripMenuItem.Text = "Delete User";
            this.deleteUserToolStripMenuItem.Click += new System.EventHandler(this.DeleteUserToolStripMenuItem_Click);
            // 
            // updateUserToolStripMenuItem
            // 
            this.updateUserToolStripMenuItem.Name = "updateUserToolStripMenuItem";
            this.updateUserToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.updateUserToolStripMenuItem.Text = "Update User";
            this.updateUserToolStripMenuItem.Click += new System.EventHandler(this.UpdateUserToolStripMenuItem_Click);
            // 
            // checkForIncomingMessagesToolStripMenuItem
            // 
            this.checkForIncomingMessagesToolStripMenuItem.Name = "checkForIncomingMessagesToolStripMenuItem";
            this.checkForIncomingMessagesToolStripMenuItem.Size = new System.Drawing.Size(280, 26);
            this.checkForIncomingMessagesToolStripMenuItem.Text = "Check for incoming messages";
            this.checkForIncomingMessagesToolStripMenuItem.Click += new System.EventHandler(this.CheckForIncomingMessagesToolStripMenuItem_Click);
            // 
            // dgvDisplay
            // 
            this.dgvDisplay.AllowUserToAddRows = false;
            this.dgvDisplay.AllowUserToDeleteRows = false;
            this.dgvDisplay.AllowUserToOrderColumns = true;
            this.dgvDisplay.AllowUserToResizeColumns = false;
            this.dgvDisplay.AllowUserToResizeRows = false;
            this.dgvDisplay.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvDisplay.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDisplay.Location = new System.Drawing.Point(13, 32);
            this.dgvDisplay.MultiSelect = false;
            this.dgvDisplay.Name = "dgvDisplay";
            this.dgvDisplay.ReadOnly = true;
            this.dgvDisplay.RowTemplate.Height = 24;
            this.dgvDisplay.Size = new System.Drawing.Size(815, 306);
            this.dgvDisplay.TabIndex = 1;
            this.dgvDisplay.SelectionChanged += new System.EventHandler(this.DgvDisplay_SelectionChanged);
            // 
            // btnFirst
            // 
            this.btnFirst.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFirst.Location = new System.Drawing.Point(6, 31);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(194, 50);
            this.btnFirst.TabIndex = 2;
            this.btnFirst.Text = "|<";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.BtnFirst_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevious.Location = new System.Drawing.Point(206, 31);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(190, 50);
            this.btnPrevious.TabIndex = 3;
            this.btnPrevious.Text = "<";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.BtnPrevious_Click);
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Location = new System.Drawing.Point(413, 30);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(190, 50);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // btnLast
            // 
            this.btnLast.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLast.Location = new System.Drawing.Point(620, 30);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(195, 50);
            this.btnLast.TabIndex = 5;
            this.btnLast.Text = ">|";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.BtnLast_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(6, 40);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(45, 17);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "Name";
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(5, 68);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(65, 17);
            this.lblSurname.TabIndex = 7;
            this.lblSurname.Text = "Surname";
            // 
            // lblRanking
            // 
            this.lblRanking.AutoSize = true;
            this.lblRanking.Location = new System.Drawing.Point(6, 124);
            this.lblRanking.Name = "lblRanking";
            this.lblRanking.Size = new System.Drawing.Size(60, 17);
            this.lblRanking.TabIndex = 8;
            this.lblRanking.Text = "Ranking";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(6, 152);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(56, 17);
            this.lblGender.TabIndex = 9;
            this.lblGender.Text = "Gender";
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Location = new System.Drawing.Point(6, 180);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(33, 17);
            this.lblAge.TabIndex = 10;
            this.lblAge.Text = "Age";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(6, 96);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(73, 17);
            this.lblID.TabIndex = 11;
            this.lblID.Text = "Id Number";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(134, 35);
            this.txtName.MaxLength = 50;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(332, 22);
            this.txtName.TabIndex = 12;
            this.txtName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtName_KeyPress);
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(134, 63);
            this.txtSurname.MaxLength = 50;
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Size = new System.Drawing.Size(332, 22);
            this.txtSurname.TabIndex = 13;
            this.txtSurname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSurname_KeyPress);
            // 
            // txtIdNumber
            // 
            this.txtIdNumber.Location = new System.Drawing.Point(134, 91);
            this.txtIdNumber.MaxLength = 13;
            this.txtIdNumber.Name = "txtIdNumber";
            this.txtIdNumber.Size = new System.Drawing.Size(332, 22);
            this.txtIdNumber.TabIndex = 14;
            this.txtIdNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtIdNumber_KeyPress);
            // 
            // txtAge
            // 
            this.txtAge.Location = new System.Drawing.Point(134, 175);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(332, 22);
            this.txtAge.TabIndex = 17;
            // 
            // gbxMoveBetweenRecords
            // 
            this.gbxMoveBetweenRecords.Controls.Add(this.btnFirst);
            this.gbxMoveBetweenRecords.Controls.Add(this.btnPrevious);
            this.gbxMoveBetweenRecords.Controls.Add(this.btnNext);
            this.gbxMoveBetweenRecords.Controls.Add(this.btnLast);
            this.gbxMoveBetweenRecords.Location = new System.Drawing.Point(13, 353);
            this.gbxMoveBetweenRecords.Name = "gbxMoveBetweenRecords";
            this.gbxMoveBetweenRecords.Size = new System.Drawing.Size(815, 100);
            this.gbxMoveBetweenRecords.TabIndex = 18;
            this.gbxMoveBetweenRecords.TabStop = false;
            this.gbxMoveBetweenRecords.Text = "View Records";
            this.gbxMoveBetweenRecords.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // gbxRecordDetails
            // 
            this.gbxRecordDetails.Controls.Add(this.btnClear);
            this.gbxRecordDetails.Controls.Add(this.txtPassword);
            this.gbxRecordDetails.Controls.Add(this.txtUsername);
            this.gbxRecordDetails.Controls.Add(this.lblUserName);
            this.gbxRecordDetails.Controls.Add(this.lblPassword);
            this.gbxRecordDetails.Controls.Add(this.lblName);
            this.gbxRecordDetails.Controls.Add(this.lblSurname);
            this.gbxRecordDetails.Controls.Add(this.txtAge);
            this.gbxRecordDetails.Controls.Add(this.lblRanking);
            this.gbxRecordDetails.Controls.Add(this.lblID);
            this.gbxRecordDetails.Controls.Add(this.txtGender);
            this.gbxRecordDetails.Controls.Add(this.lblGender);
            this.gbxRecordDetails.Controls.Add(this.txtRanking);
            this.gbxRecordDetails.Controls.Add(this.lblAge);
            this.gbxRecordDetails.Controls.Add(this.txtIdNumber);
            this.gbxRecordDetails.Controls.Add(this.txtSurname);
            this.gbxRecordDetails.Controls.Add(this.txtName);
            this.gbxRecordDetails.Location = new System.Drawing.Point(834, 32);
            this.gbxRecordDetails.Name = "gbxRecordDetails";
            this.gbxRecordDetails.Size = new System.Drawing.Size(507, 301);
            this.gbxRecordDetails.TabIndex = 19;
            this.gbxRecordDetails.TabStop = false;
            this.gbxRecordDetails.Text = "Record Details";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(134, 259);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(332, 36);
            this.btnClear.TabIndex = 22;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(134, 231);
            this.txtPassword.MaxLength = 50;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(332, 22);
            this.txtPassword.TabIndex = 21;
            this.txtPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtPassword_KeyPress);
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(134, 203);
            this.txtUsername.MaxLength = 50;
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(332, 22);
            this.txtUsername.TabIndex = 20;
            this.txtUsername.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtUsername_KeyPress);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(6, 208);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(73, 17);
            this.lblUserName.TabIndex = 18;
            this.lblUserName.Text = "Username";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(6, 236);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(69, 17);
            this.lblPassword.TabIndex = 19;
            this.lblPassword.Text = "Password";
            // 
            // txtGender
            // 
            this.txtGender.Location = new System.Drawing.Point(134, 147);
            this.txtGender.Name = "txtGender";
            this.txtGender.Size = new System.Drawing.Size(332, 22);
            this.txtGender.TabIndex = 16;
            // 
            // txtRanking
            // 
            this.txtRanking.Location = new System.Drawing.Point(134, 119);
            this.txtRanking.MaxLength = 50;
            this.txtRanking.Name = "txtRanking";
            this.txtRanking.Size = new System.Drawing.Size(332, 22);
            this.txtRanking.TabIndex = 15;
            this.txtRanking.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtRanking_KeyPress);
            // 
            // gbxFilter
            // 
            this.gbxFilter.Controls.Add(this.btnResetFilter);
            this.gbxFilter.Controls.Add(this.btnFilter);
            this.gbxFilter.Controls.Add(this.txtFilterAgeEnd);
            this.gbxFilter.Controls.Add(this.txtFilterAgeStart);
            this.gbxFilter.Controls.Add(this.lblFilterAgeEnd);
            this.gbxFilter.Controls.Add(this.lbFilterAgeStart);
            this.gbxFilter.Controls.Add(this.cbxFilterRanking);
            this.gbxFilter.Controls.Add(this.cbxFilterGender);
            this.gbxFilter.Controls.Add(this.lblFilterAge);
            this.gbxFilter.Controls.Add(this.lblFilterRanking);
            this.gbxFilter.Controls.Add(this.lblFilterGender);
            this.gbxFilter.Location = new System.Drawing.Point(834, 339);
            this.gbxFilter.Name = "gbxFilter";
            this.gbxFilter.Size = new System.Drawing.Size(507, 297);
            this.gbxFilter.TabIndex = 20;
            this.gbxFilter.TabStop = false;
            this.gbxFilter.Text = "Filter Records";
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(9, 249);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(126, 33);
            this.btnFilter.TabIndex = 9;
            this.btnFilter.Text = "Filter Records";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.BtnFilter_Click);
            // 
            // txtFilterAgeEnd
            // 
            this.txtFilterAgeEnd.Location = new System.Drawing.Point(110, 210);
            this.txtFilterAgeEnd.MaxLength = 3;
            this.txtFilterAgeEnd.Name = "txtFilterAgeEnd";
            this.txtFilterAgeEnd.Size = new System.Drawing.Size(43, 22);
            this.txtFilterAgeEnd.TabIndex = 8;
            this.txtFilterAgeEnd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtFilterAgeEnd_KeyPress);
            // 
            // txtFilterAgeStart
            // 
            this.txtFilterAgeStart.Location = new System.Drawing.Point(8, 210);
            this.txtFilterAgeStart.MaxLength = 2;
            this.txtFilterAgeStart.Name = "txtFilterAgeStart";
            this.txtFilterAgeStart.Size = new System.Drawing.Size(43, 22);
            this.txtFilterAgeStart.TabIndex = 7;
            this.txtFilterAgeStart.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtFilterAgeStart_KeyPress);
            // 
            // lblFilterAgeEnd
            // 
            this.lblFilterAgeEnd.AutoSize = true;
            this.lblFilterAgeEnd.Location = new System.Drawing.Point(107, 189);
            this.lblFilterAgeEnd.Name = "lblFilterAgeEnd";
            this.lblFilterAgeEnd.Size = new System.Drawing.Size(80, 17);
            this.lblFilterAgeEnd.TabIndex = 6;
            this.lblFilterAgeEnd.Text = "Ending age";
            // 
            // lbFilterAgeStart
            // 
            this.lbFilterAgeStart.AutoSize = true;
            this.lbFilterAgeStart.Location = new System.Drawing.Point(7, 189);
            this.lbFilterAgeStart.Name = "lbFilterAgeStart";
            this.lbFilterAgeStart.Size = new System.Drawing.Size(85, 17);
            this.lbFilterAgeStart.TabIndex = 5;
            this.lbFilterAgeStart.Text = "Starting age";
            // 
            // cbxFilterRanking
            // 
            this.cbxFilterRanking.FormattingEnabled = true;
            this.cbxFilterRanking.Location = new System.Drawing.Point(8, 127);
            this.cbxFilterRanking.Name = "cbxFilterRanking";
            this.cbxFilterRanking.Size = new System.Drawing.Size(365, 24);
            this.cbxFilterRanking.TabIndex = 4;
            this.cbxFilterRanking.SelectionChangeCommitted += new System.EventHandler(this.CbxFilterRanking_SelectionChangeCommitted);
            // 
            // cbxFilterGender
            // 
            this.cbxFilterGender.FormattingEnabled = true;
            this.cbxFilterGender.Location = new System.Drawing.Point(9, 63);
            this.cbxFilterGender.Name = "cbxFilterGender";
            this.cbxFilterGender.Size = new System.Drawing.Size(364, 24);
            this.cbxFilterGender.TabIndex = 3;
            this.cbxFilterGender.SelectionChangeCommitted += new System.EventHandler(this.CbxFilterGender_SelectionChangeCommitted);
            // 
            // lblFilterAge
            // 
            this.lblFilterAge.AutoSize = true;
            this.lblFilterAge.Location = new System.Drawing.Point(6, 168);
            this.lblFilterAge.Name = "lblFilterAge";
            this.lblFilterAge.Size = new System.Drawing.Size(53, 17);
            this.lblFilterAge.TabIndex = 2;
            this.lblFilterAge.Text = "By Age";
            // 
            // lblFilterRanking
            // 
            this.lblFilterRanking.AutoSize = true;
            this.lblFilterRanking.Location = new System.Drawing.Point(5, 106);
            this.lblFilterRanking.Name = "lblFilterRanking";
            this.lblFilterRanking.Size = new System.Drawing.Size(80, 17);
            this.lblFilterRanking.TabIndex = 1;
            this.lblFilterRanking.Text = "By Ranking";
            // 
            // lblFilterGender
            // 
            this.lblFilterGender.AutoSize = true;
            this.lblFilterGender.Location = new System.Drawing.Point(9, 42);
            this.lblFilterGender.Name = "lblFilterGender";
            this.lblFilterGender.Size = new System.Drawing.Size(76, 17);
            this.lblFilterGender.TabIndex = 0;
            this.lblFilterGender.Text = "By Gender";
            // 
            // btnExitApplication
            // 
            this.btnExitApplication.Location = new System.Drawing.Point(1240, 642);
            this.btnExitApplication.Name = "btnExitApplication";
            this.btnExitApplication.Size = new System.Drawing.Size(165, 33);
            this.btnExitApplication.TabIndex = 21;
            this.btnExitApplication.Text = "Exit Application";
            this.btnExitApplication.UseVisualStyleBackColor = true;
            this.btnExitApplication.Click += new System.EventHandler(this.BtnExitApplication_Click);
            // 
            // gbxSearch
            // 
            this.gbxSearch.Controls.Add(this.bntSearch);
            this.gbxSearch.Controls.Add(this.txtSearch);
            this.gbxSearch.Controls.Add(this.lblSearch);
            this.gbxSearch.Location = new System.Drawing.Point(19, 467);
            this.gbxSearch.Name = "gbxSearch";
            this.gbxSearch.Size = new System.Drawing.Size(390, 123);
            this.gbxSearch.TabIndex = 22;
            this.gbxSearch.TabStop = false;
            this.gbxSearch.Text = "Search";
            // 
            // bntSearch
            // 
            this.bntSearch.Location = new System.Drawing.Point(7, 89);
            this.bntSearch.Name = "bntSearch";
            this.bntSearch.Size = new System.Drawing.Size(380, 28);
            this.bntSearch.TabIndex = 2;
            this.bntSearch.Text = "Search for user";
            this.bntSearch.UseVisualStyleBackColor = true;
            this.bntSearch.Click += new System.EventHandler(this.BntSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(9, 60);
            this.txtSearch.MaxLength = 50;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(378, 22);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextChanged += new System.EventHandler(this.TxtSearch_TextChanged);
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSearch_KeyPress);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(6, 39);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(381, 17);
            this.lblSearch.TabIndex = 0;
            this.lblSearch.Text = "Please enter the first name of the user you are searcing for";
            // 
            // btnResetFilter
            // 
            this.btnResetFilter.Location = new System.Drawing.Point(141, 249);
            this.btnResetFilter.Name = "btnResetFilter";
            this.btnResetFilter.Size = new System.Drawing.Size(126, 33);
            this.btnResetFilter.TabIndex = 10;
            this.btnResetFilter.Text = "Reset Filter ";
            this.btnResetFilter.UseVisualStyleBackColor = true;
            this.btnResetFilter.Click += new System.EventHandler(this.BtnResetFilter_Click);
            // 
            // frmEditUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1409, 687);
            this.Controls.Add(this.gbxSearch);
            this.Controls.Add(this.btnExitApplication);
            this.Controls.Add(this.gbxFilter);
            this.Controls.Add(this.gbxRecordDetails);
            this.Controls.Add(this.gbxMoveBetweenRecords);
            this.Controls.Add(this.dgvDisplay);
            this.Controls.Add(this.msOptions);
            this.MainMenuStrip = this.msOptions;
            this.Name = "frmEditUsers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Users";
            this.Load += new System.EventHandler(this.FrmEditUsers_Load);
            this.msOptions.ResumeLayout(false);
            this.msOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDisplay)).EndInit();
            this.gbxMoveBetweenRecords.ResumeLayout(false);
            this.gbxRecordDetails.ResumeLayout(false);
            this.gbxRecordDetails.PerformLayout();
            this.gbxFilter.ResumeLayout(false);
            this.gbxFilter.PerformLayout();
            this.gbxSearch.ResumeLayout(false);
            this.gbxSearch.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msOptions;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkForIncomingMessagesToolStripMenuItem;
        private System.Windows.Forms.DataGridView dgvDisplay;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblRanking;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.TextBox txtIdNumber;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.GroupBox gbxMoveBetweenRecords;
        private System.Windows.Forms.GroupBox gbxRecordDetails;
        private System.Windows.Forms.TextBox txtGender;
        private System.Windows.Forms.TextBox txtRanking;
        private System.Windows.Forms.GroupBox gbxFilter;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.TextBox txtFilterAgeEnd;
        private System.Windows.Forms.TextBox txtFilterAgeStart;
        private System.Windows.Forms.Label lblFilterAgeEnd;
        private System.Windows.Forms.Label lbFilterAgeStart;
        private System.Windows.Forms.ComboBox cbxFilterRanking;
        private System.Windows.Forms.ComboBox cbxFilterGender;
        private System.Windows.Forms.Label lblFilterAge;
        private System.Windows.Forms.Label lblFilterRanking;
        private System.Windows.Forms.Label lblFilterGender;
        private System.Windows.Forms.Button btnExitApplication;
        private System.Windows.Forms.GroupBox gbxSearch;
        private System.Windows.Forms.Button bntSearch;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnResetFilter;
    }
}