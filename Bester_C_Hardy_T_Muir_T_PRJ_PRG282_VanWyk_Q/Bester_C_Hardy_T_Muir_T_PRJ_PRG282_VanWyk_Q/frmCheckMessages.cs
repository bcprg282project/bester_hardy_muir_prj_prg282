﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bester_C_Hardy_T_Muir_T_PRJ_PRG282_VanWyk_Q
{
    public partial class frmCheckMessages : Form
    {
        public frmCheckMessages()
        {
            InitializeComponent();
        }

        //created overload method of the constructor to check if
        //user may edit the users, if not the option is disabled
        public frmCheckMessages(string rank)
        {
            InitializeComponent();
            if (rank=="Officer")
            {
                editUsersToolStripMenuItem.Enabled = true;
            }
            else
            {
                editUsersToolStripMenuItem.Enabled = false;
            }
        }

        private void EditUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Instantiate object and
            //hides the current form and takes user to the new form
            frmEditUsers frmEditUsers = new frmEditUsers();
            frmEditUsers.Show();
            this.Hide();
        }

        private void DisplayHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Size = new Size(1110, 505);
            this.CenterToScreen();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnBrowse_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = dlgOpenFile.ShowDialog();
            if (dialogResult.ToString().ToUpper() == "OK")
            {
                txtLoaction.Text = dlgOpenFile.FileName;
                btnStartAnalyzin.Enabled = true;
            }
        }

        private void FrmCheckMessages_Load(object sender, EventArgs e)
        {
            btnStartAnalyzin.Enabled = false;
            this.Size = new Size(610, 505);
            this.CenterToScreen();
        }

        //Unused events
        private void OptionsToolStripMenuItem_Click(object sender, EventArgs e) { }
    }
}
