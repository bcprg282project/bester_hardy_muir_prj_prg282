﻿namespace Bester_C_Hardy_T_Muir_T_PRJ_PRG282_VanWyk_Q
{
    partial class frmCheckMessages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtLoaction = new System.Windows.Forms.TextBox();
            this.lblFilepath = new System.Windows.Forms.Label();
            this.btnStartAnalyzin = new System.Windows.Forms.Button();
            this.dgvDisplayMessages = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editUsersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lstHistory = new System.Windows.Forms.ListBox();
            this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDisplayMessages)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(541, 63);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(242, 32);
            this.btnBrowse.TabIndex = 0;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.BtnBrowse_Click);
            // 
            // txtLoaction
            // 
            this.txtLoaction.Location = new System.Drawing.Point(12, 68);
            this.txtLoaction.Name = "txtLoaction";
            this.txtLoaction.ReadOnly = true;
            this.txtLoaction.Size = new System.Drawing.Size(523, 22);
            this.txtLoaction.TabIndex = 1;
            // 
            // lblFilepath
            // 
            this.lblFilepath.AutoSize = true;
            this.lblFilepath.Location = new System.Drawing.Point(13, 39);
            this.lblFilepath.Name = "lblFilepath";
            this.lblFilepath.Size = new System.Drawing.Size(146, 17);
            this.lblFilepath.TabIndex = 2;
            this.lblFilepath.Text = "Please select filepath:";
            // 
            // btnStartAnalyzin
            // 
            this.btnStartAnalyzin.Location = new System.Drawing.Point(12, 96);
            this.btnStartAnalyzin.Name = "btnStartAnalyzin";
            this.btnStartAnalyzin.Size = new System.Drawing.Size(227, 32);
            this.btnStartAnalyzin.TabIndex = 3;
            this.btnStartAnalyzin.Text = "Start analyzing message";
            this.btnStartAnalyzin.UseVisualStyleBackColor = true;
            // 
            // dgvDisplayMessages
            // 
            this.dgvDisplayMessages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDisplayMessages.Location = new System.Drawing.Point(12, 156);
            this.dgvDisplayMessages.Name = "dgvDisplayMessages";
            this.dgvDisplayMessages.RowTemplate.Height = 24;
            this.dgvDisplayMessages.Size = new System.Drawing.Size(771, 340);
            this.dgvDisplayMessages.TabIndex = 4;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(12, 535);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(227, 32);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Exit Application";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(790, 28);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editUsersToolStripMenuItem,
            this.displayHistoryToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(73, 24);
            this.optionsToolStripMenuItem.Text = "Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.OptionsToolStripMenuItem_Click);
            // 
            // editUsersToolStripMenuItem
            // 
            this.editUsersToolStripMenuItem.Name = "editUsersToolStripMenuItem";
            this.editUsersToolStripMenuItem.Size = new System.Drawing.Size(184, 26);
            this.editUsersToolStripMenuItem.Text = "Edit Users";
            this.editUsersToolStripMenuItem.Click += new System.EventHandler(this.EditUsersToolStripMenuItem_Click);
            // 
            // displayHistoryToolStripMenuItem
            // 
            this.displayHistoryToolStripMenuItem.Name = "displayHistoryToolStripMenuItem";
            this.displayHistoryToolStripMenuItem.Size = new System.Drawing.Size(184, 26);
            this.displayHistoryToolStripMenuItem.Text = "Display History";
            this.displayHistoryToolStripMenuItem.Click += new System.EventHandler(this.DisplayHistoryToolStripMenuItem_Click);
            // 
            // lstHistory
            // 
            this.lstHistory.FormattingEnabled = true;
            this.lstHistory.ItemHeight = 16;
            this.lstHistory.Location = new System.Drawing.Point(789, 60);
            this.lstHistory.Name = "lstHistory";
            this.lstHistory.Size = new System.Drawing.Size(656, 436);
            this.lstHistory.TabIndex = 8;
            // 
            // frmCheckMessages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 574);
            this.Controls.Add(this.lstHistory);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dgvDisplayMessages);
            this.Controls.Add(this.btnStartAnalyzin);
            this.Controls.Add(this.lblFilepath);
            this.Controls.Add(this.txtLoaction);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmCheckMessages";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Check for incoming messages";
            this.Load += new System.EventHandler(this.FrmCheckMessages_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDisplayMessages)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtLoaction;
        private System.Windows.Forms.Label lblFilepath;
        private System.Windows.Forms.Button btnStartAnalyzin;
        private System.Windows.Forms.DataGridView dgvDisplayMessages;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editUsersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayHistoryToolStripMenuItem;
        private System.Windows.Forms.ListBox lstHistory;
        private System.Windows.Forms.OpenFileDialog dlgOpenFile;
    }
}