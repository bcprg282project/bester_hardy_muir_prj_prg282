USE [CommunicationDB]
GO
/****** Object:  StoredProcedure [dbo].[InsertUserData]    Script Date: 2019/05/08 09:22:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[InsertUserData] 
	-- Add the parameters for the stored procedure here
	@firstName varchar(50),
	@lastName varchar(50), 
	@iDNumber varchar(13),
	@currentRank varchar(50), 
	@username varchar(50),
	@password varchar(50) 
AS
BEGIN
INSERT INTO tblUser (FirstName,LastName,IDNumber,CurrentRank,Username,Password)
VALUES ( @firstName,@lastName , @iDNumber,@currentRank, @username,@password)
END
