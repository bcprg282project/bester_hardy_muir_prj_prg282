USE [CommunicationDB]
GO
/****** Object:  StoredProcedure [dbo].[UpdateData]    Script Date: 2019/05/08 10:21:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[UpdateData] 
	-- Add the parameters for the stored procedure here
	@name varchar(50), 
	@surname varchar(50),
	@ranking varchar(50), 
	@username varchar(50),
	@password varchar(50),
	@userID int

AS
BEGIN
UPDATE tblUser
SET 
FirstName = @name,
LastName = @surname,
CurrentRank = @ranking,
Username = @username,
Password=@password
where 
UserID=@userID
END
