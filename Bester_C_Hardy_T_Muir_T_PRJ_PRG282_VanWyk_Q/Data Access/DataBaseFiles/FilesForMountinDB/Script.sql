USE [master]
GO
/****** Object:  Database [CommunicationDB]    Script Date: 2019/05/08 18:24:30 ******/
CREATE DATABASE [CommunicationDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CommunicationDB', FILENAME = N'D:\Program Files\MSSQL14.MSSQLSERVER\MSSQL\DATA\CommunicationDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CommunicationDB_log', FILENAME = N'D:\Program Files\MSSQL14.MSSQLSERVER\MSSQL\DATA\CommunicationDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CommunicationDB] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CommunicationDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CommunicationDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CommunicationDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CommunicationDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CommunicationDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CommunicationDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [CommunicationDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CommunicationDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CommunicationDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CommunicationDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CommunicationDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CommunicationDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CommunicationDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CommunicationDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CommunicationDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CommunicationDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CommunicationDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CommunicationDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CommunicationDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CommunicationDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CommunicationDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CommunicationDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CommunicationDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CommunicationDB] SET RECOVERY FULL 
GO
ALTER DATABASE [CommunicationDB] SET  MULTI_USER 
GO
ALTER DATABASE [CommunicationDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CommunicationDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CommunicationDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CommunicationDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CommunicationDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'CommunicationDB', N'ON'
GO
ALTER DATABASE [CommunicationDB] SET QUERY_STORE = OFF
GO
USE [CommunicationDB]
GO
/****** Object:  Table [dbo].[Log]    Script Date: 2019/05/08 18:24:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[MessageID] [int] NOT NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Message]    Script Date: 2019/05/08 18:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Message](
	[MessageID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[DecryptedMessage] [varchar](100) NULL,
	[TimeToDecrypt] [datetime] NULL,
 CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED 
(
	[MessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUser]    Script Date: 2019/05/08 18:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUser](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[IDNumber] [varchar](13) NULL,
	[CurrentRank] [nvarchar](50) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblUser] ON 

INSERT [dbo].[tblUser] ([UserID], [FirstName], [LastName], [IDNumber], [CurrentRank], [Username], [Password]) VALUES (1, N'Charl', N'Bester', N'7704075055085', N'Officer', N'admin', N'admin1')
INSERT [dbo].[tblUser] ([UserID], [FirstName], [LastName], [IDNumber], [CurrentRank], [Username], [Password]) VALUES (2, N'Tristan', N'Muir', N'7602145088085', N'Officer', N'admin', N'admin3')
INSERT [dbo].[tblUser] ([UserID], [FirstName], [LastName], [IDNumber], [CurrentRank], [Username], [Password]) VALUES (3, N'Tennille', N'Hardy', N'7112124044085', N'Officer', N'admin', N'admin2')
INSERT [dbo].[tblUser] ([UserID], [FirstName], [LastName], [IDNumber], [CurrentRank], [Username], [Password]) VALUES (4, N'Morne', N'Smit', N'7812305044085', N'Recruit', N'mSmit78', N'password123')
INSERT [dbo].[tblUser] ([UserID], [FirstName], [LastName], [IDNumber], [CurrentRank], [Username], [Password]) VALUES (8, N'Christie', N'Bode', N'7412244999085', N'Recruit', N'cBode74', N'password123')
SET IDENTITY_INSERT [dbo].[tblUser] OFF
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[tblUser] ([UserID])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_User]
GO
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[tblUser] ([UserID])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_User]
GO
/****** Object:  StoredProcedure [dbo].[DeleteUser]    Script Date: 2019/05/08 18:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteUser] 
	-- Add the parameters for the stored procedure here
	@user_ID int
AS
BEGIN
DELETE FROM tblUser WHERE UserID=@user_ID
END
GO
/****** Object:  StoredProcedure [dbo].[InsertUserData]    Script Date: 2019/05/08 18:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertUserData] 
	-- Add the parameters for the stored procedure here
	@firstName varchar(50),
	@lastName varchar(50), 
	@iDNumber varchar(13),
	@currentRank varchar(50), 
	@username varchar(50),
	@password varchar(50) 
AS
BEGIN
INSERT INTO tblUser (FirstName,LastName,IDNumber,CurrentRank,Username,Password)
VALUES ( @firstName,@lastName , @iDNumber,@currentRank, @username,@password)
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateData]    Script Date: 2019/05/08 18:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateData] 
	-- Add the parameters for the stored procedure here
	@name varchar(50), 
	@surname varchar(50),
	@ranking varchar(50), 
	@username varchar(50),
	@password varchar(50),
	@userID int

AS
BEGIN
UPDATE tblUser
SET 
FirstName = @name,
LastName = @surname,
CurrentRank = @ranking,
Username = @username,
Password=@password
where 
UserID=@userID
END
GO
USE [master]
GO
ALTER DATABASE [CommunicationDB] SET  READ_WRITE 
GO
