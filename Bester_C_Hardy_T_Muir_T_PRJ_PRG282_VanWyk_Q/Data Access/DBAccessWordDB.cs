﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Bester_C_Hardy_T_Muir_T_PRJ_PRG282_VanWyk_Q
{
    public class DBAccessWordDB
    {
        //Data Source=TRISTAN-PC\TRISTANSERVER2;Initial Catalog=words;Integrated Security=True
        SqlConnectionStringBuilder connection = new SqlConnectionStringBuilder();

        public DBAccessWordDB()
        {
            connection.DataSource = @"TRISTAN-PC\TRISTANSERVER2";
            connection.InitialCatalog = "WordDB";
            connection.IntegratedSecurity = true;
        }

        public DataSet ReadData(string tblname)
        {
            DataSet rawData = new DataSet();
            SqlConnection conn = new SqlConnection(connection.ToString());
            string qry = string.Format("SELECT * FROM {0}", tblname);
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(qry, conn);
                adapter.FillSchema(rawData, SchemaType.Source, tblname);
                adapter.Fill(rawData, tblname);
            }
            catch (SqlException se)
            {

                MessageBox.Show(se.ToString());
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return rawData;
        }


        public DataSet ReadDataByLength(string tblname,int length)
        {
            DataSet rawData = new DataSet();
            SqlConnection conn = new SqlConnection(connection.ToString());
            string qry = string.Format("SELECT * FROM {0} WHERE LEN(Word) = {1} ORDER BY Word", tblname,length);
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(qry, conn);
                adapter.FillSchema(rawData, SchemaType.Source, tblname);
                adapter.Fill(rawData, tblname);
            }
            catch (SqlException se)
            {

                MessageBox.Show(se.ToString());
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return rawData;
        }
    }
}

