﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Data_Access
{
    public class DBAccessCommunicationDB
    {
        //Data Source=CHARLBESTERASUS;Initial Catalog=CommunicationDB;Integrated Security=True
        SqlConnectionStringBuilder SqlConnectionString = new SqlConnectionStringBuilder();
        public DBAccessCommunicationDB()
        {
            SqlConnectionString.DataSource = @"CHARLBESTERASUS";
            SqlConnectionString.InitialCatalog = "CommunicationDB";
            SqlConnectionString.IntegratedSecurity = true;
        }

        public DataSet ReadAllData(string tableName)
        {
            DataSet rawData = new DataSet();
            using (SqlConnection sqlConnection = new SqlConnection(SqlConnectionString.ToString()))
            {
                string readQry = string.Format("SELECT * FROM {0}", tableName);
                sqlConnection.Open();
                using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(readQry, sqlConnection))
                {
                    sqlDataAdapter.FillSchema(rawData, SchemaType.Source, tableName);
                    sqlDataAdapter.Fill(rawData, tableName);
                }
            }
            return rawData;
        }

        public void InsertData(string fName, string lName, string iDNum, string rank, string userName, string password)
        {
            using (SqlConnection sqlConnection = new SqlConnection(SqlConnectionString.ToString()))
            {
                sqlConnection.Open();
                using (SqlCommand command = new SqlCommand("InsertUserData", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@firstName", fName);
                    command.Parameters.AddWithValue("@lastName", lName);
                    command.Parameters.AddWithValue("@iDNumber", iDNum);
                    command.Parameters.AddWithValue("@currentRank", rank);
                    command.Parameters.AddWithValue("@username", userName);
                    command.Parameters.AddWithValue("@password", password);

                    command.ExecuteNonQuery();
                }
            }
        }

        public void UpdateData(int id,string fName, string lName, string rank, string userName, string password)
        {
            using (SqlConnection sqlConnection = new SqlConnection(SqlConnectionString.ToString()))
            {
                sqlConnection.Open();
                using (SqlCommand command = new SqlCommand("UpdateData", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@userID", id);
                    command.Parameters.AddWithValue("@name", fName);
                    command.Parameters.AddWithValue("@surname", lName);
                    command.Parameters.AddWithValue("@ranking", rank);
                    command.Parameters.AddWithValue("@username", userName);
                    command.Parameters.AddWithValue("@password", password);

                    command.ExecuteNonQuery();
                }
            }
        }

        public void DeleteData(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(SqlConnectionString.ToString()))
            {
                sqlConnection.Open();
                using (SqlCommand command = new SqlCommand("DeleteUser", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@user_ID", id);

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
