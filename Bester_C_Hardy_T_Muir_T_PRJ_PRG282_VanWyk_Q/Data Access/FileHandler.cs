﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Bester_C_Hardy_T_Muir_T_PRJ_PRG282_VanWyk_Q
{
    public class FileHandler
    {
        string path;
        FileStream stream;
        StreamReader reader;
        StreamWriter writer;

        public FileHandler(string pathname = "PRG282EncryptedBinarySampleData.txt")
        {
            this.path = pathname;
        }

       public List<string> ReadFile()
       {
            List<string> lstMessage = new List<string>();
            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                reader = new StreamReader(stream);
                while (!reader.EndOfStream)
                {
                    lstMessage.Add(reader.ReadLine());
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                reader.Close();
                stream.Close();
            }

            return lstMessage;

       }

        public void WriteFile(string towrite)
        {
            try
            {
                stream = new FileStream(path, FileMode.Append, FileAccess.Write);
                writer = new StreamWriter(stream);
                
                writer.WriteLine(towrite);
            }
            catch (FileNotFoundException)
            {


            }
            catch (IOException)
            {

            }
            finally
            {
                writer.Close();
                stream.Close();

            }

        }
        public List<string> ConvertBiToAscii()
        {
            //list of the binary message from the text file
            List<string> lstBinary = this.ReadFile();
            //list of the converted textfile lines
            List<string> lstAscii = new List<string>();
            //char list that will hold the converted binary char
            List<char> lstMessage = new List<char>();
           
            string result ;
            string message="";
            foreach (string msg in lstBinary)
            {
                result = msg;
                while (result.Length > 0)
                {
                    //selects the first 8 bits of binary
                    string first8 = result.Substring(0, 8);
                    //removes the first 8 bits of binary
                    result = result.Substring(8);
                    //converts the binary base 2 to a integer
                    int number = Convert.ToInt32(first8, 2);
                    //implicit convertion from int to char, every character has an integer value in Ascii
                    lstMessage.Add((char)((number)));
                }
                foreach (char item in lstMessage)
                {
                    message = message + item.ToString();
                }
                lstMessage.Clear();
                
                lstAscii.Add(message);
                //reset string
                message = "";


            }

            

            return lstAscii;
        }


    }
}
