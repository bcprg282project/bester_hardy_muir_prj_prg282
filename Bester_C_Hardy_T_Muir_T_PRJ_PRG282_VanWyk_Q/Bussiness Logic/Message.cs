﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bussiness_Logic
{
    class Message
    {
        ////fileds and properties
        private int messageid;
        private int userid;
        private string decryptedmessage;
        private int timetodecrypt;

        public int Messageid { get => messageid; set => messageid = value; }
        public int Userid { get => userid; set => userid = value; }
        public string Decryptedmessage { get => decryptedmessage; set => decryptedmessage = value; }
        public int Timetodecrypt { get => timetodecrypt; set => timetodecrypt = value; }

        ////constructor
        public Message(string pdecryptedmessage, int ptimetodecrypt)
        {

            this.Decryptedmessage = pdecryptedmessage;
            this.Timetodecrypt = ptimetodecrypt;
        }

        public Message()
        {

        }

        //this is the breakup message method 
        public Message messageobjectname(string messageinput)
        {

            int time;


            messageinput = messageinput.Substring(20);

            string[] data = messageinput.Split('#');

            string decmessage = data[0];

            time = int.Parse(data[1]);

            Message messageobj = new Message(decmessage, time);


            return messageobj;

        }
    }
}
