﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;
using Data_Access;

namespace Bester_C_Hardy_T_Muir_T_PRJ_PRG282_VanWyk_Q
{
    class CaeserCipher
    {
        private FileHandler fh = new FileHandler();
        public CaeserCipher()
        {

        }

        /*
         Algorithm For Encryption

            For each plaintext (P), Ciphertext (C) is.

                C = E (3, P) = (P+3) mod 26

         For Decryption

                P = D (3, C) = (C-3) mod 26

                In general  [C= E (K, P) = (P+K) mod 26]
                [P = D (K, C) = (C-K) mod 26], where 
                    P	Plaintext
                    C	Ciphertext
                    E	Encryption Algorithm
                    D	Decryption Algorithm
                    K	Key
             */


        public char Cipher(char ch, int key)
        {
            //Caeser Cipher only works with the Alphabet
            if (!char.IsLetter(ch))
            {

                return ch;
            }

            char d = char.IsUpper(ch) ? 'A' : 'a';
            return (char)((((ch + key) - d) % 26) + d);


        }
        //Encrypts each character of the method
        public string Encipher(string input, int key)
        {
            string output = string.Empty;

            foreach (char ch in input)
                output += Cipher(ch, key);

            return output;
        }

        public string Decipher(string input, int key)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Format("Invalid Message");
            }
            else if (key == 100)
            {
                return string.Format("{0} Invalid Message", input.Substring(0, 20));
            }
            else
            {
                return Encipher(input, 26 - key);
            }

        }


        //Gets most commenly used words from WordDB and populates a list
        public List<string> PopulateWords(int length)
        {
            List<string> lstWords = new List<string>();
            DBAccessWordDB db = new DBAccessWordDB();
            DataSet ds = db.ReadDataByLength("tblWord", length);
            foreach (DataRow item in ds.Tables["tblWord"].Rows)
            {
                lstWords.Add(item["Word"].ToString());
            }

            return lstWords;
        }
        //Decrypts the first word using a key and check if that word is in the WordDB
        //increments key if no match is found
        //moves to the next word if no match is found
        public int GetKey(string message)
        {

            string[] words = message.Split(' ');


            int cipherKey = 0;
            bool stop = false;

            foreach (string item in words)
            {
                List<string> lstWords = PopulateWords(item.Length);

                foreach (string word in lstWords)
                {
                    for (int key = 0; key < 26; key++)
                    {
                        if (Decipher(item, key) == word)
                        {
                            cipherKey = key;
                            stop = true;
                        }
                    }
                    if (stop)
                    {
                        break;

                    }

                }
                if (stop)
                {
                    break;
                }

            }

            if (!stop)
            {
                return 100;
            }
            //////////////////////////////////////////////////////////////////
            return cipherKey;
        }

        public List<string> DecryptMessage()
        {
            //populates a list with the encrypted ascii lines from the textfile
            List<string> Lstencrypted = fh.ConvertBiToAscii();
            List<string> dec = new List<string>();
            string message;
            int key;
            foreach (string item in Lstencrypted)
            {
                Stopwatch watch = Stopwatch.StartNew();
                key = GetKey(item);
                message = Decipher(item, key);

                watch.Stop();
                long elapsedMs = watch.ElapsedMilliseconds;
                dec.Add(string.Format("{0}-{1}", message, elapsedMs.ToString()));
            }


            return dec;

        }

    }
}
