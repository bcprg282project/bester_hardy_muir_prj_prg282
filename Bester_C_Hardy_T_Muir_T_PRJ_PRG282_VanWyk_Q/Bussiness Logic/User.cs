﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data_Access;
using System.Data;
using System.Windows.Forms;

namespace Bussiness_Logic
{
    public class User
    {
        private int userID;
        private string firstName;
        private string lastName;
        private string iDNumber;
        private string currentRank;
        private int age;
        private string gender;
        private string username;
        private string password;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string IDNumber { get => iDNumber; set => iDNumber = value; }
        public string CurrentRank { get => currentRank; set => currentRank = value; }
        public int Age { get => age; set => age = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Gender { get => gender; set => gender = value; }
        public int UserID { get => userID; set => userID = value; }

        public User(string firstName, string lastName, string iDNumber, string currentRank, string username, string password, int userID)
        {
            this.userID = userID;
            this.firstName = firstName;
            this.lastName = lastName;
            this.iDNumber = iDNumber;
            this.currentRank = currentRank;
            this.age = GetAge();
            this.gender = GetGender();
            this.username = username;
            this.password = password;
        }

        //Method to determne each each user's age, takes current year minus the birthyear
        public int GetAge()
        {
            string line = (iDNumber.Substring(0, 6));
            int birthYear = int.Parse(line.Substring(0, 2));
            if (birthYear < 99)
            {
                birthYear = birthYear + 1900;
            }
            else
            {
                birthYear = birthYear + 2000;
            }
            //Constant
            int currentYear = 2019;

            return currentYear - birthYear;
        }

        //Method to determine teh gender of each person
        public string GetGender()
        {
            string gender = iDNumber.Substring(6, 4);
            if (int.Parse(gender) > 5000)
            {
                return "Male";
            }
            else
            {
                return "Female";
            }
        }

        public User() { }

        List<User> UserList = new List<User>();
        public List<User> ReadData()
        {

            DBAccessCommunicationDB dBAccessCommunicationDB = new DBAccessCommunicationDB();
            DataSet ds = dBAccessCommunicationDB.ReadAllData("tblUser");
            foreach (DataRow item in ds.Tables["tblUser"].Rows)
            {
                UserList.Add(
                    new User(item["FirstName"].ToString(),
                    item["LastName"].ToString(),
                    item["IDNumber"].ToString(),
                    item["CurrentRank"].ToString(),
                    item["Username"].ToString(),
                    item["Password"].ToString(),
                    int.Parse(item["UserID"].ToString())));
            }
            return UserList;
        }

        public void InsertData(string fName, string lName, string iDNum, string rank, string userName, string password)
        {
            DBAccessCommunicationDB dBAccessCommunicationDB = new DBAccessCommunicationDB();
            ReadData();
            bool flag = true;
            foreach (User item in UserList)
            {
                if (item.IDNumber==iDNum)
                {
                    MessageBox.Show("ID numbers can't match ","Insert error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    flag = false;
                    break;
                }
            }
            if (flag)
            {
                dBAccessCommunicationDB.InsertData(fName, lName, iDNum, rank, userName, password);
                MessageBox.Show("New user has been added", "Insert Succesful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void DeleteData(int id)
        {
            DBAccessCommunicationDB dBAccessCommunicationDB = new DBAccessCommunicationDB();
        
            bool flag = true;
            ReadData();
            foreach (User item in UserList)
            {
                if (item.userID==id)
                {
                    MessageBox.Show("Can't delete own user", "Delete unuccesful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    flag = false;
                    break;
                }
            }

            if (flag)
            {
                dBAccessCommunicationDB.DeleteData(id);
                MessageBox.Show("User has been deleted", "Delete Succesful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void UpdateData(int id, string fName, string lName, string rank, string userName, string password)
        {
            DBAccessCommunicationDB dBAccessCommunicationDB = new DBAccessCommunicationDB();
            dBAccessCommunicationDB.UpdateData(id, fName, lName, rank, userName, password);
            MessageBox.Show("User has been updated", "Update Succesful", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public List<User> FilterData(int startAge, int endAge, string ranking, string gender)
        {
            ReadData();
            List<User> RawList = new List<User>();

            foreach (User item in UserList)
            {
                if ((item.age >= startAge && item.age <= endAge)&&(item.currentRank == ranking) &&(item.gender == gender))
                {
                    RawList.Add(item);
                }
            }

            return RawList;
        }

        public List<User> SearchData( string searchText)
        {
            ReadData();
            List<User> RawList = new List<User>();

            foreach (User item in UserList)
            {
                if (item.FirstName.ToUpper().Contains(searchText))
                {
                    RawList.Add(item);
                }
            }
            return RawList;
        }
    }
}
