﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bussiness_Logic
{
    class Log
    {
        private int stamp;
        private int currentUserID;
        private int messageID;

        public int Stamp { get => stamp; set => stamp = 1; }
        public int CurrentUserID { get => currentUserID; set => currentUserID = value; }
        public int MessageID { get => messageID; set => messageID = value; }

        public Log(int userid,int messageid)
        {
            this.currentUserID = userid;
            this.messageID = messageid;
        }

        
    }
}
